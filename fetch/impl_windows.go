// +build windows
package fetch

import (
	"fmt"
	"github.com/StackExchange/wmi"
)

func NetworkAdapters() ([]NetworkAdapter, error) {
	var adapters []NetworkAdapter
	var win32_adapters []win32_NetworkAdapter
	query := wmi.CreateQuery(&win32_adapters, "")
	err := wmi.Query(query, &win32_adapters)
	if err != nil {
		return nil, err
	}
	for _, win32_adapter := range win32_adapters {
		var config []win32_NetworkAdapterConfiguration
		query := wmi.CreateQuery(&config,
			fmt.Sprintf("WHERE InterfaceIndex = %v",
				win32_adapter.InterfaceIndex))
		err := wmi.Query(query, &config)
		if err != nil {
			return nil, err
		}
		if len(config) == 0 {
			return nil, BadWMIResponse("Win32_NetworkAdapterConfiguration returned 0 items")
		}
		var t NetworkAdapter
		t.Name = win32_adapter.Name
		t.IPs = config[0].IPAddress
		t.Subnets = config[0].IPSubnet
		t.Gateways = config[0].DefaultIPGateway
		adapters = append(adapters, t)
	}

	return adapters, nil
}

func InstalledPrograms() ([]InstalledProgram, error) {
	var programs []InstalledProgram
	err := wmi.Query("SELECT Caption,InstallLocation,Name,Version FROM Win32_Product", &programs)
	if err != nil {
		return nil, err
	}
	return programs, nil
}

func SystemInfo() (SystemInformation, error) {
	var info SystemInformation

	var win32_os []win32_OperatingSystem
	query := wmi.CreateQuery(&win32_os, "")
	err := wmi.Query(query, &win32_os)
	if err != nil {
		return info, err
	}

	if len(win32_os) == 0 {
		return info, BadWMIResponse("Win32_OperatingSystem returned 0 items")
	}

	var win32_pc []win32_ComputerSystem
	query = wmi.CreateQuery(&win32_pc, "")
	err = wmi.Query(query, &win32_pc)
	if err != nil {
		return info, err
	}

	if len(win32_pc) == 0 {
		return info, BadWMIResponse("Win32_ComputerSystem returned 0 items")
	}

	info.OSBuild = win32_os[0].BuildNumber
	info.OSLocale = win32_os[0].Locale
	info.OSName = win32_os[0].Caption
	info.OSVersion = win32_os[0].Version
	info.OSVersionExtra = fmt.Sprintf("%v.%v", win32_os[0].ServicePackMajorVersion, win32_os[0].ServicePackMinorVersion)
	info.Domain = win32_pc[0].Domain
	info.DomainRole = win32_pc[0].DomainRole
	info.PCName = win32_pc[0].Name
	info.PartOfDomain = win32_pc[0].PartOfDomain
	info.Workgroup = win32_pc[0].Workgroup

	return info, nil
}

func Users() ([]User, error) {
	var users []User
	err := wmi.Query("SELECT FullName,Name,LastLogon,NumberOfLogons,Flags FROM Win32_NetworkLoginProfile", &users)
	if err != nil {
		return nil, err
	}
	return users, nil
}
