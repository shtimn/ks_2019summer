package fetch

import (
	"encoding/json"
	ps_host "github.com/shirou/gopsutil/host"
	ps_net "github.com/shirou/gopsutil/net"
	ps_proc "github.com/shirou/gopsutil/process"
)

func Json() ([]byte, error) {
	val, err := json.Marshal(Summary())
	return val, err
}

func Summary() SummaryInfo {
	var ret SummaryInfo

	ch_basic_sysinfo := make(chan Result)
	ch_sysinfo := make(chan Result)
	ch_proc := make(chan Result)
	ch_conn := make(chan Result)
	ch_net := make(chan Result)
	ch_prog := make(chan Result)
	ch_user := make(chan Result)

	go BasicSystemInfoAsync(ch_basic_sysinfo)
	go SystemInfoAsync(ch_sysinfo)
	go ProcessesAsync(ch_proc)
	go ConnectionsAsync(ch_conn)
	go NetworkAdaptersAsync(ch_net)
	go InstalledProgramsAsync(ch_prog)
	go UsersAsync(ch_user)

	waiting := 7

	for {
		select {
		case info := <-ch_basic_sysinfo:
			ret.BasicSystemInfo = info.Value.(BasicSystemInformation)
			ret.Errors.BasicSystemInfo = info.Error
			ch_basic_sysinfo = nil
			waiting--

		case info := <-ch_sysinfo:
			ret.SystemInfo = info.Value.(SystemInformation)
			ret.Errors.SystemInfo = info.Error
			ch_sysinfo = nil
			waiting--

		case info := <-ch_proc:
			ret.Processes = info.Value.([]Process)
			ret.Errors.Processes = info.Error
			ch_proc = nil
			waiting--

		case info := <-ch_conn:
			ret.Connections = info.Value.([]Connection)
			ret.Errors.Connections = info.Error
			ch_conn = nil
			waiting--

		case info := <-ch_net:
			ret.NetworkAdapters = info.Value.([]NetworkAdapter)
			ret.Errors.NetworkAdapters = info.Error
			ch_net = nil
			waiting--

		case info := <-ch_prog:
			ret.InstalledPrograms = info.Value.([]InstalledProgram)
			ret.Errors.InstalledPrograms = info.Error
			ch_prog = nil
			waiting--

		case info := <-ch_user:
			ret.Users = info.Value.([]User)
			ret.Errors.Users = info.Error
			ch_user = nil
			waiting--
		}

		if waiting == 0 {
			break
		}
	}

	return ret
}

func SystemInfoAsync(ch chan Result) {
	val, err := SystemInfo()
	ch <- Result{val, err}
}

func NetworkAdaptersAsync(ch chan Result) {
	val, err := NetworkAdapters()
	ch <- Result{val, err}
}

func InstalledProgramsAsync(ch chan Result) {
	val, err := InstalledPrograms()
	ch <- Result{val, err}
}

func UsersAsync(ch chan Result) {
	val, err := Users()
	ch <- Result{val, err}
}

func BasicSystemInfo() (BasicSystemInformation, error) {
	var ret BasicSystemInformation
	host_info, err := ps_host.Info()
	if err != nil {
		return ret, err
	}
	ret.OS = host_info.OS
	ret.OSFamily = host_info.PlatformFamily
	return ret, nil
}

func BasicSystemInfoAsync(ch chan Result) {
	val, err := BasicSystemInfo()
	ch <- Result{val, err}
}

// TODO: doc (https://godoc.org/github.com/shirou/gopsutil/process#Process.Status) & err
func Processes() ([]Process, error) {
	var ret []Process
	procs, err := ps_proc.Processes()
	if err != nil {
		return nil, err
	}

	for _, proc := range procs {
		var tmp Process
		tmp.Pid = proc.Pid

		name, err := proc.Name()
		if err != nil {
			name = "?"
		}

		load, err := proc.CPUPercent()
		if err != nil {
			load = -1.0
		}

		path, err := proc.Exe()
		if err != nil {
			path = "?"
		}

		args, err := proc.Cmdline()
		if err != nil {
			args = "?"
		}

		stat, err := proc.Status()
		if err != nil {
			stat = "?"
		}

		tmp.Name = name
		tmp.Load = load
		tmp.Path = path
		tmp.Cmdline = args
		tmp.Status = stat

		ret = append(ret, tmp)
	}
	return ret, nil
}

func ProcessesAsync(ch chan Result) {
	val, err := Processes()
	ch <- Result{val, err}
}

func Connections() ([]Connection, error) {
	var ret []Connection
	conns, err := ps_net.Connections("all")
	if err != nil {
		return nil, err
	}

	for _, conn := range conns {
		if conn.Status == "NONE" || conn.Status == "" {
			continue
		}
		var tmp Connection
		tmp.Status = conn.Status
		tmp.ProcessPid = conn.Pid
		proc, err := ps_proc.NewProcess(conn.Pid)
		name := "?"
		if err == nil {
			name, _ = proc.Name()
		}
		tmp.ProcessName = name
		tmp.LocalAddress = conn.Laddr
		tmp.RemoteAddress = conn.Raddr

		ret = append(ret, tmp)
	}
	return ret, nil
}

func ConnectionsAsync(ch chan Result) {
	val, err := Connections()
	ch <- Result{val, err}
}
