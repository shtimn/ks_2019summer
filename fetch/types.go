package fetch

import (
	ps_net "github.com/shirou/gopsutil/net"
)

// Internal types for wmi.CreateQuery

type win32_NetworkAdapter struct {
	Name           string
	InterfaceIndex uint32
}

type win32_NetworkAdapterConfiguration struct {
	DefaultIPGateway []string
	IPAddress        []string
	IPSubnet         []string
	InterfaceIndex   uint32
}

type win32_Product struct {
	Caption string
}

type win32_OperatingSystem struct {
	BuildNumber             string
	BuildType               string
	Caption                 string
	Locale                  string
	Version                 string
	ServicePackMajorVersion uint16
	ServicePackMinorVersion uint16
}

// TODO: map DomainRole values
type win32_ComputerSystem struct {
	Domain       string
	DomainRole   uint16
	Name         string
	PartOfDomain bool
	Workgroup    string
}

// types that will be returned

type NetworkAdapter struct {
	Name     string
	Gateways []string
	IPs      []string
	Subnets  []string
}

type InstalledProgram struct {
	Caption         string
	InstallLocation string
	Name            string
	Version         string
}

type Process struct {
	Cmdline string
	Pid     int32
	Path    string
	Name    string
	Status  string
	Load    float64
}

type Connection struct {
	ProcessPid    int32
	ProcessName   string
	LocalAddress  ps_net.Addr
	RemoteAddress ps_net.Addr
	Status        string
}

type SystemInformation struct {
	OSBuild        string
	OSLocale       string
	OSName         string
	OSVersion      string
	OSVersionExtra string
	Domain         string
	DomainRole     uint16
	PCName         string
	PartOfDomain   bool
	Workgroup      string
}

type BasicSystemInformation struct {
	OS       string
	OSFamily string
}

// TODO: get info from `Flags`
type User struct {
	FullName       string
	Name           string
	LastLogon      string
	NumberOfLogons uint32
	Flags          uint32
}

type Errors struct {
	SystemInfo        error
	BasicSystemInfo   error
	NetworkAdapters   error
	InstalledPrograms error
	Users             error
	Processes         error
	Connections       error
}

type SummaryInfo struct {
	Errors            Errors
	BasicSystemInfo   BasicSystemInformation
	SystemInfo        SystemInformation
	NetworkAdapters   []NetworkAdapter
	InstalledPrograms []InstalledProgram
	Users             []User
	Processes         []Process
	Connections       []Connection
}

// error types

type BadWMIResponse string

func (e BadWMIResponse) Error() string {
	return "Bad WMI Response: " + string(e)
}

type SomethingWentWrong bool

func (e SomethingWentWrong) Error() string {
	return "Something went wrong, check Errors property."
}

// for goroutines
type Result struct {
	Value interface{}
	Error error
}
